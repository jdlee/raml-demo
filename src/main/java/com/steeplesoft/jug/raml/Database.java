/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.steeplesoft.jug.raml;

import com.steeplesoft.jug.raml.model.Author;
import com.steeplesoft.jug.raml.model.Authors;
import com.steeplesoft.jug.raml.model.Book;
import com.steeplesoft.jug.raml.model.Books;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * @author jdlee
 */
public final class Database {
    private final AtomicInteger authorPK = new AtomicInteger();
    private final AtomicInteger bookPK = new AtomicInteger();
    
    private final Map<Integer, Author> authors = new HashMap<>();
    private final Map<Integer, Book> books = new HashMap<>();
    
    private static class LazyHolder {
        static final Database INSTANCE = new Database();
    }
    
    private Database() {
        Author jordan = addAuthor(new Author().withName("Robert Jordan"));
        for (int i = 1; i <= 14; i++) {
            Book book = new Book().withName("Wheel of Time " + i).withAuthorId(jordan.getId());
            addBook(book);
            jordan.getBooks().add(book);
        }
    }
    
    public static Database getInstance() {
        return LazyHolder.INSTANCE;
    }

    public Authors getAuthors() {
        final ArrayList<Author> list = new ArrayList<>(authors.values());
        return new Authors().withAuthors(list).withSize(list.size());
    }
    
    public List<Author> getAuthorList() {
        return new ArrayList<>(authors.values());
    }
    
    public Author getAuthor(int id) {
        return authors.get(id);
    }
    
    public Author addAuthor(Author author) {
        final int id = authorPK.incrementAndGet();
        author.setId(id);
        authors.put(id, author);
        
        return author;
    }
    
    public void deleteAuthor(int id) {
        authors.remove(id);
    }
    
    public void updateAuthor(Author author) {
        if (!authors.containsKey(author.getId())) {
            throw new RuntimeException("No such author");
        }
        
        authors.put(author.getId(), author);
    }

    public Books getBooks() {
        final ArrayList<Book> list = new ArrayList<>(books.values());
        return new Books().withBooks(list).withSize(list.size());
    }
    
    public Book getBook(int id) {
        return books.get(id);
    }
    
    public Book addBook(Book book) {
        final int id = bookPK.incrementAndGet();
        book.setId(id);
        books.put(id, book);
        
        return book;
    }
    
    public void deleteBook(int id) {
        authors.remove(id);
    }
    
    public void updateBook(Book book) {
        if (!books.containsKey(book.getId())) {
            throw new RuntimeException("No such book");
        }
        
        books.put(book.getId(), book);
    }
}
