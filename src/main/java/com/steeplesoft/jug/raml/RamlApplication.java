/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.steeplesoft.jug.raml;

import com.steeplesoft.jug.raml.resource.impl.AuthorsResourceImpl;
import com.steeplesoft.jug.raml.resource.impl.BooksResourceImpl;
import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 *
 * @author jdlee
 */
@ApplicationPath("resources")
public class RamlApplication extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> s = new HashSet<>();
        s.add(AuthorsResourceImpl.class);
        s.add(BooksResourceImpl.class);
        return s;
    }
}
