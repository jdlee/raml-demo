/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.steeplesoft.jug.raml.resource.impl;

import com.steeplesoft.jug.raml.Database;
import com.steeplesoft.jug.raml.model.Author;
import com.steeplesoft.jug.raml.model.Authors;
import com.steeplesoft.jug.raml.resource.AuthorsResource;

/**
 *
 * @author jdlee
 */
public class AuthorsResourceImpl implements AuthorsResource {

    @Override
    public GetAuthorsResponse getAuthors(String authorization, String query, Long start, Long pages) throws Exception {
        return GetAuthorsResponse.jsonOK(Database.getInstance().getAuthors());
    }

    @Override
    public PostAuthorsResponse postAuthors(String authorization, Authors entity) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public GetAuthorsIdByAuthorIdResponse getAuthorsIdByAuthorId(Long authorId, String authorization) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public PostAuthorsIdByAuthorIdResponse postAuthorsIdByAuthorId(Long authorId, String authorization, Author entity) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void deleteAuthorsIdByAuthorId(Long authorId, String authorization) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
