/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.steeplesoft.jug.raml.resource.impl;

import com.steeplesoft.jug.raml.Database;
import com.steeplesoft.jug.raml.model.Book;
import com.steeplesoft.jug.raml.model.Books;
import com.steeplesoft.jug.raml.resource.BooksResource;

/**
 *
 * @author jdlee
 */
public class BooksResourceImpl implements BooksResource {

    @Override
    public GetBooksResponse getBooks(String authorization, Long start, Long pages) throws Exception {
        return GetBooksResponse.jsonOK(Database.getInstance().getBooks());
    }

    @Override
    public PostBooksResponse postBooks(String authorization, Books entity) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public GetBooksIdByBookIdResponse getBooksIdByBookId(Long bookId, String authorization) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public PostBooksIdByBookIdResponse postBooksIdByBookId(Long bookId, String authorization, Book entity) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void deleteBooksIdByBookId(Long bookId, String authorization) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
